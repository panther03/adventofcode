import sys
import math
from enum import Enum
from operator import itemgetter

class Line:

    def __init__(self,x0,y0,dx,dy):
        self.x0 = x0
        self.y0 = y0
        self.dx = dx
        self.dy = dy

    def __str__(self):
        return f"{self.x0},{self.y0},{self.dx},{self.dy}"

    def points(self):
        if self.dx == 0:
            return list(zip([self.x0]*(self.dy+1),list(range(self.y0,self.y0+self.dy+1))))
        elif self.dy == 0:
            return list(zip(list(range(self.x0,self.x0+self.dx+1)),[self.y0]*(self.dx+1)))
        else:
            print("A fatal error has occured. Please delete system32")
            return None

def intersects(l1, l2):
    intersections = []
    d = l1.points()
    for p in d:
        if (l2.x0 <= p[0] <= l2.x0 + l2.dx) and (l2.y0 <= p[1] <= l2.y0 + l2.dy):
            intersections.append(p)
    #print(intersections)
    return intersections

def decompose(w):
    (x,y) = (0,0)
    wl = []
    for step in w:
        m = int(step[1:])
        if step[0] == "U":
            wl.append(Line(x,y,0,m))
            (x,y) = (x,y+m)
        elif step[0] == "D":
            wl.append(Line(x,y-m,0,m))
            (x,y) = (x,y-m)
        elif step[0] == "R":
            wl.append(Line(x,y,m,0))
            (x,y) = (x+m,y)
        elif step[0] == "L":
            wl.append(Line(x-m,y,m,0))
            (x,y) = (x-m,y)
        else:
            continue
    return wl

def traverse(w,intersections):
    (x,y) = (0,0)
    paths = []
    steps = 0
    for step in w:
        m = int(step[1:])
        (xt,yt) = (x,y)
        if step[0] == "U":
            while yt < y + m:
                yt += 1
                steps += 1
                match = [p for p in intersections if p == (xt,yt)]
                if match:
                    paths.append((steps,match[0]))
            (x,y) = (x,y+m)
        elif step[0] == "D":
            while yt > y - m:
                yt -= 1
                steps += 1
                match = [p for p in intersections if p == (xt,yt)]
                if match:
                    paths.append((steps,match[0]))
            (x,y) = (x,y-m)
        elif step[0] == "R":
            while xt < x + m:
                xt += 1
                steps += 1
                match = [p for p in intersections if p == (xt,yt)]
                if match:
                    paths.append((steps,match[0]))
            (x,y) = (x+m,y)
        elif step[0] == "L":
            while xt > x - m:
                xt -= 1
                steps += 1
                match = [p for p in intersections if p == (xt,yt)]
                if match:
                    paths.append((steps,match[0]))
            (x,y) = (x-m,y)
        else:
            continue
    return paths

def p1(w1,w2, all = False):
    wl1 = decompose(w1)
    wl2 = decompose(w2)
    intersections = []
    for l1 in wl1:
        for l2 in wl2:
            intersections += intersects(l1,l2)
    if all: 
        return intersections[1:]
    else:
        return min(list(map(lambda x: abs(x[0])+abs(x[1]),intersections[1:])))

def p2(w1,w2):
    intersections = p1(w1,w2,all=True)
    return min(list(map(lambda x: x[0][0] + x[1][0],list(zip(sorted(traverse(w1,intersections),key=itemgetter(1)),sorted(traverse(w2,intersections),key=itemgetter(1)))))))
    # I apologize to God for this one-liner from hell.

# debug method
def graph(w1,w2):
    l = [['.' for i in range(300)] for j in range(300)] #[['.']*50]*50
    for l1 in decompose(w1):
        for point1 in l1.points():
            print(point1)
            l[point1[1]][point1[0]] = '-'
    for l2 in decompose(w2):
        for point2 in l2.points():
            l[point2[1]][point2[0]] = '_'
    for i in p1(w1,w2):
        l[i[1]][i[0]] = 'x'
    for x in l:
        for y in x:
            print(y,end='')
        print('')
    
if __name__ == "__main__":
    w1 = sys.stdin.readline().split(sep=",")
    w2 = sys.stdin.readline().split(sep=",")
    print(p2(w1,w2))