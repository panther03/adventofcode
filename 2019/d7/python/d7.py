import sys
from functools import reduce
from operator import itemgetter

def d5_p1(l,inps):
    i = 0
    num_input = 0
    out = 0
    while i < len(l):
        op = str(l[i]).zfill(4)
        if int(op[2:]) == 99:
            break
        elif int(op[2:]) == 3:
            l[l[i+1]] = inps[num_input]
            num_input += 1
            i += 2
        elif int(op[2:]) == 4:
            out = l[l[i+1]] if int(op[0]) == 0 else l[i+1]
            i += 2
        elif int(op[2:]) in [1,2,5,6,7,8]:
            m1,m2 = int(op[1]),int(op[0])
            p1,p2 = l[i+1] if m1 == 1 else l[l[i+1]],l[i+2] if m2 == 1 else l[l[i+2]]
            #print(l[i+1],l[i+2])
            #print(l[l[i+1]],l[l[i+2]])
            if int(op[2:]) == 1:
                l[l[i+3]] = p1 + p2
            elif int(op[2:]) == 2:
                l[l[i+3]] = p1 * p2
            elif int(op[2:]) == 5:
                if p1 != 0:
                    i = p2
                    continue
                else:
                    i += 3
                    continue
            elif int(op[2:]) == 6:
                if p1 == 0:
                    i = p2
                    continue
                else:
                    i += 3
                    continue
            elif int(op[2:]) == 7:
                if p1 < p2:
                    l[l[i+3]] = 1
                else:
                    l[l[i+3]] = 0
            elif int(op[2:]) == 8:
                if p1 == p2:
                    l[l[i+3]] = 1
                else:
                    l[l[i+3]] = 0
            i += 4
        else:
            print(f"opcode {l[i]} not recognized")
            
    return out

def d7_p1(l,i = 0):
    combinations = []
    for x in set(range(5)):
        for y in set(range(5)) - {x}:
            for z in set(range(5)) - {x,y}:
                for w in set(range(5)) - {x,y,z}:
                    for v in set(range(5)) - {x,y,z,w}:
                        ss = [x,y,z,w,v]
                        fuck = d5_p1(l,[v,d5_p1(l,[w,d5_p1(l,[z,d5_p1(l,[y,d5_p1(l,[x,0])])])])])
                        print(ss, fuck)
                        combinations.append((ss,fuck))
    return reduce(lambda x,y: str(x) + str(y),max(combinations, key = itemgetter(1)))


if __name__ == "__main__":
    l = list(map(lambda x: int(x),sys.stdin.readline().split(sep=",")))
    print(d7_p1(l))