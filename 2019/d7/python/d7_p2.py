import sys
from functools import reduce
from operator import itemgetter

def d5_modified(l,inps, i):
    num_input = 0
    out = 0
    while i < len(l):
        op = str(l[i]).zfill(4)
        #print("fuck: ",i, op, l)
        if int(op[2:]) == 99:
            break
        elif int(op[2:]) == 3:
            num_input = l[:i].count(3)
            #print("op Fufck fufck KILL ME I HATE ADVENT OF CODE",num_input,inps)
            if num_input >= len(inps):
                return (3,out,l,i)
            else:
                l[l[i+1]] = inps[num_input]
                i += 2
        elif int(op[2:]) == 4:
            out = l[l[i+1]] if int(op[0]) == 0 else l[i+1]
            i += 2
            return (4,out,l,i)
        elif int(op[2:]) in [1,2,5,6,7,8]:
            m1,m2 = int(op[1]),int(op[0])
            p1,p2 = l[i+1] if m1 == 1 else l[l[i+1]],l[i+2] if m2 == 1 else l[l[i+2]]
            #print(p1,p2)
            #print(l[i+1],l[i+2])
            #print(l[l[i+1]],l[l[i+2]])
            if int(op[2:]) == 1:
                l[l[i+3]] = p1 + p2
            elif int(op[2:]) == 2:
                l[l[i+3]] = p1 * p2
            elif int(op[2:]) == 5:
                if p1 != 0:
                    i = p2
                    continue
                else:
                    i += 3
                    continue
            elif int(op[2:]) == 6:
                if p1 == 0:
                    i = p2
                    continue
                else:
                    i += 3
                    continue
            elif int(op[2:]) == 7:
                if p1 < p2:
                    l[l[i+3]] = 1
                else:
                    l[l[i+3]] = 0
            elif int(op[2:]) == 8:
                if p1 == p2:
                    l[l[i+3]] = 1
                else:
                    l[l[i+3]] = 0
            i += 4
        else:
            print(f"opcode {l[i]} not recognized")
            
    return (99,out,l,i)

def d7_p2(l):
    combinations = []
    print(len(l))
    for x in set(range(5,10)):
        for y in set(range(5,10)) - {x}:
            for z in set(range(5,10)) - {x,y}:
                for w in set(range(5,10)) - {x,y,z}:
                    for v in set(range(5,10)) - {x,y,z,w}:
                        ai = 0 # Which amplifier?
                        ss = [x,y,z,w,v] # Store amplifier setting
                        iss = [0,0,0,0,0] # Store last memory position
                        ls = [l,l,l,l,l]
                        os = []
                        o,ot,oe = 0,0,0 # Last output code, last output type (4 or 99.)
                        print(ss)
                        while ot != 99:
                            print(ai,ot, o, iss[ai],os,ls[ai])
                            os.insert(0,o)
                            os = os[:(l.count(3)-1)]
                            poopfart = d5_modified(ls[ai].copy(),[ss[ai]] + os,iss[ai])
                            if poopfart[0] == 3:
                                ai = (ai + 1) % 5
                                continue    
                            else:
                                (ot,o,ls[ai],iss[ai]) = poopfart
                            print(ai,ot, o, iss[ai],os,ls[ai])
                            if ai == 4:
                                oe = o
                            ai = (ai + 1) % 5
                        print(ls)
                        print(ai,ot, o, iss[ai],ls[ai])
                        combinations.append((ss,oe))
    return reduce(lambda x,y: str(x) + str(y),max(combinations, key = itemgetter(1)))


if __name__ == "__main__":
    l = list(map(lambda x: int(x),sys.stdin.readline().split(sep=",")))
    print(d7_p2(l))