import sys
import copy
# Import the necessary packages and modules
import matplotlib.pyplot as plt
import numpy as np



# This method MODIFIES vels
def gravitate(ci,pos,vels):
    for i in range(len(pos)):
        if i != ci:
            if pos[i][0] > pos[ci][0]:
                vels[ci][0] += 1
            elif pos[i][0] == pos[ci][0]:
                pass
            else:
                vels[ci][0] -= 1
            
            if pos[i][1] > pos[ci][1]:
                vels[ci][1] += 1
            elif pos[i][1] == pos[ci][1]:
                pass
            else:
                vels[ci][1] -= 1
            if pos[i][2] > pos[ci][2]:
                vels[ci][2] += 1
            elif pos[i][2] == pos[ci][2]:
                pass
            else:
                vels[ci][2] -= 1

def energy(pv):
    return (abs(pv[0][0])+abs(pv[0][1])+abs(pv[0][2]))

def simulate(pos,tmax):
    t = 0
    vels = [[0,0,0] for x in range(len(pos))]
    while t < tmax:
        print(t)
        for ci in range(len(pos)):
            gravitate(ci,pos,vels)
        for ci in range(len(pos)):
            pos[ci][0] += vels[ci][0]
            pos[ci][1] += vels[ci][1]
            pos[ci][2] += vels[ci][2]
        print(pos,vels)
        t += 1
    return pos,vels

def simulate_p2(pos,tmax):
    t = 0
    vels = [[0,0,0] for x in range(len(pos))]
    datax = []
    datay = []
    while t < tmax:
        print(t)
        for ci in range(len(pos)):
            gravitate(ci,pos,vels)
        for ci in range(len(pos)):
            pos[ci][0] += vels[ci][0]
            pos[ci][1] += vels[ci][1]
            pos[ci][2] += vels[ci][2]
        t += 1
        datay.append(sum(list(map(energy,list(zip(pos,vels))))))
        datax.append(t)
    return datax,datay

def d12_p1(pos,tmax):
    pos,vels = simulate(pos,tmax)
    return sum(list(map(energy,list(zip(pos,vels)))))

if __name__ == "__main__":
    pos = [[4,1,1],[11,-18,-1],[-2,-10,-4],[-7,-2,14]]
    pos2 = [[-8,-10,0],[5,5,10],[2,-7,3],[9,-8,-3]]
    pos3 = [[-1,0,2],[2,-10,-7],[4,-8,8],[3,5,-1]]
        # Prepare the data
    datax,datay = simulate_p2(pos2,3000)
    print(datax[datay.index(max(datay))])
    print(datax[2772])
    # Plot the data
    plt.plot(datax, datay, label='linear',markevery = 10,linewidth=0.5)

    # Add a legend
    plt.legend()

    # Show the plot
    plt.show()