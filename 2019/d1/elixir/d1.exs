defmodule Day1 do 
    def p1 do 
        fuel = IO.read(:stdio, :all)
            |> String.split("\n")
            |> Enum.reverse |> tl
            |> Enum.map(fn x -> Integer.parse(x) |> elem(0) |> (fn x -> div(x,3) - 2 end).() end)
        Enum.sum(fuel)
    end
    def cost(x) do
        IO.puts(x)
        y = div(x,3) - 2
        if y <= 0 do
            0
        else
            y + cost(y)
        end
    end
    def p2 do
        fuel_compound = IO.read(:stdio, :all)
            |> String.split("\n")
            |> Enum.reverse |> tl
            |> Enum.map(fn x -> Integer.parse(x) |> elem(0) |> cost() end)
        Enum.sum(fuel_compound)
    end
end

IO.puts(Day1.p2())
