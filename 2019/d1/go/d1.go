package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func part1() int {
	scanner := bufio.NewScanner(os.Stdin)
	var fuel int = 0
	for scanner.Scan() {
		m, _ := strconv.Atoi(scanner.Text())
		fuel += (m / 3) - 2

	}
	return fuel
}

func part2() int {
	scanner := bufio.NewScanner(os.Stdin)
	var fuel int = 0
	for scanner.Scan() {
		m, _ := strconv.Atoi(scanner.Text())
		n := m
		for n > 0 {
			fuel += n
			n = n/3 - 2
		}
		fuel -= m
	}
	return fuel
}

func main() {
	fmt.Println(part2())
}
