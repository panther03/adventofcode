import sys

def d1_p1(l):
    n = 0 
    for x in l:
        n += x // 3 - 2
    return n

def d1_p2(l):
    n = 0 
    for x in l:
        y = x
        while y > 0:
            n += y
            y = y // 3 - 2
        n -= x
    return n

if __name__ == "__main__": 
    l = []
    for line in sys.stdin:
        l.append(int(line))
    print(d1_p2(l))