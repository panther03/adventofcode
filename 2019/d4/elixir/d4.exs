
defmodule Day4 do
    def digit(x,n) do
        div((rem(x,:math.pow(10,n) |> round )),(:math.pow(10,n-1)) |> round )
    end
    def increasing(p) do
        digit(p,6) <= digit(p,5) and digit(p,5) <= digit(p,4) and digit(p,4) <= digit(p,3) and digit(p,3) <= digit(p,2) and digit(p,2) <= digit(p,1)
    end
    def adjacent(p) do
        digit(p,6) == digit(p,5) or digit(p,5) == digit(p,4) or digit(p,4) == digit(p,3) or digit(p,3) == digit(p,2) or digit(p,2) == digit(p,1)
    end
    def counter([],acc), do: acc
    def counter([d|ds],acc) do
        counter(ds,Map.update(acc,d,1,&(&1+1)))
    end
    def adjacent_exact(p) do 
        to_charlist(p)
        |> counter(%{})
        |> Map.values
        |> Enum.member?(2)
    end
    def p1(min,max) do
        length(for p <- min..max, adjacent(p) and increasing(p), do: 0)
    end
    def p2(min,max) do 
        length(for p <- min..max, adjacent_exact(p) and increasing(p), do: 0)
    end
end