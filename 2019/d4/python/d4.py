import sys
from collections import Counter

def increasing(p):
    return (int(p[0]) <= int(p[1])) and (int(p[1]) <= int(p[2])) and (int(p[2]) <= int(p[3])) and (int(p[3]) <= int(p[4])) and (int(p[4]) <= int(p[5]))

def adjacent(p):
    return (int(p[0]) == int(p[1])) or (int(p[1]) == int(p[2])) or (int(p[2]) == int(p[3])) or (int(p[3]) == int(p[4])) or (int(p[4]) == int(p[5]))

def adjacent_exact(p):
    return 2 in dict(Counter(p)).values()

def p2(pmin,pmax):
    return len([p for p in range(pmin,pmax) if adjacent_exact(str(p)) and increasing(str(p))])

def p1(pmin,pmax):
    return len([p for p in range(pmin,pmax) if adjacent(str(p)) and increasing(str(p))])

if __name__ == "__main__":
    pminmax = list(map(lambda x: int(x),sys.stdin.readline().split(sep="-")))
    print(p1(pminmax[0],pminmax[1]))

