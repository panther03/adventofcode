import sys

def p1(pmin,pmax):
    ps = 0
    for p in range(pmin,pmax):
        adjacent = False
        increasing = True
        for i in range(5):
            if int(str(p)[i]) == int(str(p)[i+1]):
                adjacent = True
            if int(str(p)[i]) > int(str(p)[i+1]):
                increasing = False
        if adjacent and increasing:
            print(p)
            ps += 1
    return ps

def adjacents(c,s):
    count = 0
    i = 0
    while i < len(s) and s[i] == c:
        count += 1
        i += 1
    return count

def p2(pmin,pmax):
    ps = 0
    for p in range(pmin,pmax):
        adjacent = False
        increasing = True
        i = 0
        while i < 5:
            if int(str(p)[i]) > int(str(p)[i+1]):
                increasing = False
            print(p,i,str(p)[i],str(p)[i:])
            count = adjacents(str(p)[i],str(p)[i:])
            if count > 2:
                i += count - 2
            if count == 2:
                adjacent = True
            i += 1
        if adjacent and increasing:
            print(p)
            ps += 1
    return ps

if __name__ == "__main__":
    pminmax = list(map(lambda x: int(x),sys.stdin.readline().split(sep="-")))
    print(p2(pminmax[0],pminmax[1]))

