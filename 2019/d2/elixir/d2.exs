defmodule Day2 do 
    def intcode(l, acc) do
        if acc < length(l) do
            case Enum.at(l, acc) do
                1 -> intcode(List.replace_at(l, Enum.at(l, acc + 3), Enum.at(l,Enum.at(l, acc+1)) + Enum.at(l,Enum.at(l, acc + 2))), acc + 4)
                2 -> intcode(List.replace_at(l, Enum.at(l, acc + 3), Enum.at(l,Enum.at(l, acc+1)) * Enum.at(l,Enum.at(l, acc + 2))), acc + 4)
                99 -> l
                _ -> intcode(l, acc + 4)
            end
        else l end
    end
    def match_intcode(l, xmax, ymax) do
        for x <- 0..xmax, y <- 0..ymax, Enum.at(intcode(l |> List.replace_at(1,x) |> List.replace_at(2,y),0),0) == 19690720, do: 100 * x + y
    end
    def p1 do 
        IO.read(:stdio, :all)
            |> String.split(",")
            |> Enum.map(fn x -> Integer.parse(x) |> elem(0) end)
            |> List.replace_at(1,12) |> List.replace_at(2,2)
            |> intcode(0)
            |> Enum.at(0)
    end
    def p2 do 
        IO.read(:stdio, :all)
            |> String.split(",")
            |> Enum.map(fn x -> Integer.parse(x) |> elem(0) end)
            |> match_intcode(99,99)
            |> Enum.at(0)
    end
end
IO.puts(Day2.p2())