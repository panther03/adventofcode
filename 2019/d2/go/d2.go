package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func intcode(l []int) int {
	for i := 0; i <= len(l); i += 4 {
		switch l[i] {
		case 1:
			l[l[i+3]] = l[l[i+1]] + l[l[i+2]]
		case 2:
			l[l[i+3]] = l[l[i+1]] * l[l[i+2]]
		case 99:
			return l[0]
		default:
			fmt.Println("opcode ", l[i], " at ", i, " not recognized")
		}
	}
	return l[0]
}

func part1() int {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	prgRaw := strings.Split(scanner.Text(), ",")
	prg := make([]int, len(prgRaw))
	for i, e := range prgRaw {
		prg[i], _ = strconv.Atoi(e)
	}
	prg[1] = 12
	prg[2] = 2
	return intcode(prg)
}

func part2(xmax int, ymax int) int {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	prgRaw := strings.Split(scanner.Text(), ",")
	prg := make([]int, len(prgRaw))
	for i, e := range prgRaw {
		prg[i], _ = strconv.Atoi(e)
	}
	for i := 0; i <= xmax; i++ {
		for j := 0; j <= ymax; j++ {
			prgNew := make([]int, len(prg))
			copy(prgNew, prg)
			prgNew[1], prgNew[2] = i, j
			if intcode(prgNew) == 19690720 {
				fmt.Println("solution:", i, ",", j)
				return 100*i + j
			}
		}
	}
	return -1
}

func main() {
	fmt.Println(part2(99, 99))
}
