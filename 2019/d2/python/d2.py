import sys
import time

def d2_p1(l, n, v):
    l[1] = n
    l[2] = v
    for i in range(0, len(l), 4):
        if l[i] == 1:
                l[l[i+3]] = l[l[i+1]] + l[l[i+2]]
        elif l[i] == 2:
            l[l[i+3]] = l[l[i+1]] * l[l[i+2]]
        elif l[i] == 99:
            break
        else:
            print(f"opcode {l[i]} not recognized")
    return l[0]
    
def d2_p2(l,x,y):
    start = time.time()
    for i in range(x):
        for j in range(y):
            nl = l.copy()
            n = d2_p1(nl,i,j)
            if n == 19690720:
                print(f"solution: {i},{j}")
                return 100 * i + j
    print(f"time: {time.time()-start}")

if __name__ == "__main__":
    l = list(map(lambda x: int(x),sys.stdin.readline().split(sep=",")))
    print(d2_p2(l,99,99))


