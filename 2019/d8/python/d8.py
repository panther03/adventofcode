import sys

def split_index(s,step):
    l = []
    for i in range(0,len(s),step):
        l.append(s[i:i+step])
    try:
        l.remove('\n')
    except ValueError:
        pass
    return l
    
def pixel_sum(l):
    i = 0
    while l[i] == '2' and i < len(l):
        i += 1
    if l[i] == '2':
        return 0
    return l[i]

def d8_p1(image,w,l):
    layer = min(split_index(image,w * l),key=lambda x: x.count('0'))
    print(layer)
    return layer.count('1') * layer.count('2')

def d8_p2(image,w,l):
    layers = split_index(image,w * l)
    image = ""
    for y in range(l):
        for x in range(w):
            p = pixel_sum([layer[y*w + x] for layer in layers])
            image += str(p) +  '   '
        image += '\n'
    return image

if __name__ == "__main__":
    print(d8_p2(sys.stdin.readline(),25,6))