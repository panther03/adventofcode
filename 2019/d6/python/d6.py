import sys

def tomap(raw_map):
    orbits = {}
    for orbit in raw_map:
        objects = orbit.split(')')
        orbits[objects[1]] = objects[0]
    return orbits

def traverse_path(orbits,o):
    path = []
    while o != 'COM':
        o = orbits[o]
        path.append(o)
    return path

def traverse(orbits,o):
    n = 0
    while o != 'COM':
        o = orbits[o]
        n += 1
    return n

def d6_p1(raw_map):
    orbits = tomap(raw_map)
    orbit_sum = sum([traverse(orbits,o) for o in orbits.keys()])
    
    return orbit_sum
    
def d6_p2(raw_map):
    orbits = tomap(raw_map)
    path_santa = traverse_path(orbits,'SAN')
    path_me = traverse_path(orbits,'YOU')
    common = [o for o in path_santa if o in path_me]
    return path_santa.index(common[0]) + path_me.index(common[0])
    

if __name__ == "__main__":
    raw_map = [line.replace("\n","") for line in sys.stdin]
    print(d6_p2(raw_map))