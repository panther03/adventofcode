#!/usr/bin/python3

def polymer(s):
    for i in range(len(s)-1):
        if (i <= len(s)-1) and (s[i].lower() == s[i+1].lower()):
            l = list(s)
            l[i] = ''
            l[i+1] = ''
            return polymer(''.join(l))
    return len(s)