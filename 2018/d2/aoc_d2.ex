defmodule Day2 do
    def create_map(s) do
        create_maph(String.to_charlist(s),%{})
    end

    def create_maph(sl,acc) do
        if sl == [] , do: acc, else: create_maph(tl(sl),Map.update(acc,hd(sl), 1, &(&1 + 1)))
    end

    def count_appr(s, n) do
        if Enum.empty?(Enum.filter(create_map(s), fn x -> elem(x,1) == n end)), do: 0, else: 1
    end

    def day2(ss) do
        Enum.sum(Enum.map(ss, fn x -> count_appr(x,2) end)) * Enum.sum(Enum.map(ss, fn x -> count_appr(x,3) end))
    end
end
