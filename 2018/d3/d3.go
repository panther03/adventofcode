package main

import (
	"fmt"
)

type Claim struct {
	id int
	x0 int
	y0 int
	w  int
	l  int
}

func inClaim(c Claim, x int, y int) bool {
	return (c.x0 <= x && x < c.x0+c.w) && (c.y0 <= y && y < c.y0+c.l)
}

func dimClaims(cs []Claim) (int, int) {
	maxx, maxy := 0, 0
	for _, c := range cs {
		if c.x0+c.w > maxx {
			maxx = c.x0 + c.w
		}
		if c.y0+c.l > maxy {
			maxy = c.y0 + c.l
		}
	}
	return maxx, maxy
}

func areaClaims(cs []Claim) int {
	maxx, maxy := dimClaims(cs)
	a := 0
	for i := 0; i <= maxx; i++ {
		for j := 0; j <= maxy; j++ {
			n := 0
			for _, c := range cs {
				if inClaim(c, i, j) {
					n += 1
					fmt.Println(i, j, n)
				}
			}
			if n >= 2 {
				a += 1
			}
		}
	}
	return a
}

func main() {

	claims := []Claim{
		Claim{1, 1, 3, 4, 4},
		Claim{2, 3, 1, 4, 4},
		Claim{3, 5, 5, 2, 2},
	}

	fmt.Println(areaClaims(claims))

}